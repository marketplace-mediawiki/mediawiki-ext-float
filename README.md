# Information / Информация

Выделение текста определённым цветом.

## Install / Установка

1. Загрузите папки и файлы в директорию `extensions/MW_EXT_Color`.
2. В самый низ файла `LocalSettings.php` добавьте строку:

```php
wfLoadExtension( 'MW_EXT_Color' );
```

## Syntax / Синтаксис

```html
<color type="red">[CONTENT]</color>
```

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
